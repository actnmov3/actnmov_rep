<!--
form output
	title_activity
	type_activity
	city_activity
	time_activity
	description_activity
	contact
	occurence
	level
	private_key
	submit_activity
-->

<?php

$var_security = 1; 					// security variable

if (isset($_POST['submit_activity']) or isset($_POST['save_activity'])){
	//echo 'check security query</br>';
	$username 		=	$_SESSION['username'];
	$activityID 	=	1;
	$title 			=	htmlentities($_POST['title_activity'		]			);
	$title 	= 	stripcslashes(preg_replace("/(\r\n|\n|\r)/", "<br />", $title))	;
	$type			=	htmlentities($_POST['type_activity'			]			);
	$city			=	htmlentities($_POST['city_activity'			]			);
	$time_activity	=	htmlentities($_POST['time_activity'			]			);
	$description	=	htmlentities($_POST['description_activity'	]			);
	$description0 	= 	preg_replace("/(\r\n\r\n|\n\n|\r\r)/", "<p />", $description);
	$description0 	= 	stripcslashes(preg_replace("/(\r\n|\n|\r)/", "<br />", $description));
	$contact		=	htmlentities($_POST['contact'				]			);
	$occurence		=	htmlentities($_POST['occurence'				]			);
	$level			=	htmlentities($_POST['level'					]			);
	$private_key	=	htmlentities($_POST['private_key'			]			);
	$ID				=	1				;
	if ($private_key=="")
		$status="Public";
	else $status="Private";

	$count_activity = 0;
	require 'PhpCode/24_display_activities.php';
}
elseif(isset($_POST['add_activity'])){ //Default values
	$title 			= 'Activity title'									;
	$description 	= "Description"										;
	$type			= "Sport"											;
	$city			= "Antibes"											;
	$occurence		= '1'												;
	require_once 'PhpCode/30_add_activity.php'							;
}

if (isset($_POST['save_activity'])){
	require_once 'PhpCode/login.php';
	$connection =  new mysqli($db_hostname, $db_username, $db_password, $db_database);
	if ($connection->connect_error) die($connection->connect_error);
	//echo "The number is: $x <br>";
	//$query = "INSERT INTO activity_attribute (act_org, act_ref_id, act_title, act_description, act_type, act_city, act_nb_occurence ) 
	//VALUES ('$username', '$activityID','$title','$description0', '$type', '$city', '$occurence')";
	$query = "INSERT INTO activity (
	activitytitle, 
	owner_name, 
	owner_id, 
	sport_type, 
	city, 
	description, 
	creation_time, 
	status, 
	occurence, 
	time_activity, 
	contact, 
	private_key,
	level) 
	VALUES (
	'$title', 
	'$username', 
	1,
	'$type',
	'$city',
	'$description0',
	NULL,
	'$status',
	'$occurence',
	'$time_activity',
	'$contact',
	'$private_key',
	'$level'
	)";
	$result = $connection->query($query);
	if (!$result) {die($connection->error);}
	$query="INSERT INTO history (username, action) VALUE ('$username','activity created');";
	$result = $connection->query($query);
	if (!$result) die($connection->error);
	echo "Activity added in the database<br>";
}
if (isset($_POST['submit_activity'])){
	echo "<form 	action='' method='post'>";
	echo "<input type='submit' name='save_activity' 		value='Save' class = 'controls'>" ;

	echo "<input type='hidden' name='title_activity' 		value='$title' 			>" ;
	echo "<input type='hidden' name='type_activity' 		value='$type' 			>" ;
	echo "<input type='hidden' name='city_activity' 		value='$city'  			>" ;
	echo "<input type='hidden' name='time_activity' 		value='$time_activity' 	>" ;
	echo "<input type='hidden' name='description_activity' 	value='$description' 	>" ;
	echo "<input type='hidden' name='contact' 				value='$contact' 		>" ;
	echo "<input type='hidden' name='occurence' 			value='$occurence' 		>" ;
	echo "<input type='hidden' name='level' 				value='$level' 			>" ;
	echo "<input type='hidden' name='private_key' 			value='$private_key'	>" ;
}
?>

<!--
form output
	title_activity
	type_activity
	city_activity
	time_activity
	description_activity
	contact
	occurence
	level
	private_key
	submit_activity
-->