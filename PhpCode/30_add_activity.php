<!--
form output
	title_activity
	type_activity
	city_activity
	time_activity
	description_activity
	contact
	occurence
	level
	private_key
	submit_activity
-->

<style type="text/css">
	.add_activity{
		display: flex;
		flex-direction: column;
		justify-content: flex-start;
		align-items: center;
		max-width: 100%;
	}
	.optionsactivity{
		display: flex;
		flex-direction: row;
		justify-content: flex-start;
		align-items: flex-start;
		width:100%;
	}
	.suboptionsactivity{
		display: flex;
		flex-direction: column;
		justify-content: flex-start;
		align-items: center;
		flex:1;
	}
	.formaddactivity{
	  color: #5a5854;
	  background-color: #f1f8ff;
	  border: 1px solid #61E0FF;
	  border-radius: 0px;
	  padding: 2px;
	  margin:  2px;
	  text-align: left;
	  min-width: 300px;
	  max-width: 500px;
	  width:90%;
	}
	.subformaddactivity{
	  color: #5a5854;
	  background-color: #f1f8ff;
	  border: 1px solid #61E0FF;
	  border-radius: 0px;
	  padding: 2px;
	  margin:  2px;
	  text-align: left;
	  min-width: 100px;
	  max-width: 300px;
	}
	.submitinput{
		margin: 5px;
		width:200px;
	}
</style>

<form 	action='' method='post' class="add_activity" id="add_activity">
	<label for="title_activity">Title</label>
	<input type='text' name='title_activity' placeholder="Enter title" class ='formaddactivity'></input>
	
	<label for="city_activity">City</label>
	<!--<input type='text' name='city_activity' placeholder="Enter city" class = 'formaddactivity'></input>-->
	<select name="city_activity" class = 'formaddactivity'>
	    <OPTION value=""> <?php echo "$location_list[0]" ?> </OPTION>
	    <?php
	    $length = sizeof($location_list)-1; 
	    for ($i=1; $i<=$length; $i++){
	      echo "<OPTION value= $location_list[$i]> $location_list[$i] </OPTION>";
	    }
	    ?> 
	  </select>



	
	<label for="contact">Contact URL or Email</label>
	<input type='text' name='contact' class = 'formaddactivity' placeholder="Enter contact"></input>
	
	<label style="display: none" for="time_activity">When will it take place</label>
	<input style="display: none" type='text' name='time_activity' placeholder="Enter time period" class = 'formaddactivity'></input>
	
	<label for="description_activity">Description and time activity</label>
	<textarea cols='60' rows='5' name='description_activity' class ='formaddactivity' id="description_activity" placeholder="Describe activity, time, location and cost when needed" value =""></textarea>


	<div class="optionsactivity">

		<div class="suboptionsactivity">
		<label for="type_activity">Type</label>
		<SELECT name='type_activity' class = 'subformaddactivity'>  
		    <OPTION value=""> <?php echo "$sport_list[0]" ?> </OPTION>
	        <?php
		    $length = sizeof($sport_list)-1; 
		    for ($i=1; $i<=$length; $i++){
		      echo "<OPTION value= $sport_list[$i]> $sport_list[$i] </OPTION>";
		    }
			?> 
		</SELECT>
		</div>

		<div class="suboptionsactivity">
		<label for="occurence">Occurence </label>
		<SELECT name='occurence' class = 'subformaddactivity'>  
		    <OPTION value=""> <?php echo "$occurence_list[0]" ?> </OPTION>
	        <?php
		    $length = sizeof($occurence_list)-1; 
		    for ($i=1; $i<=$length; $i++){
		      echo "<OPTION value= $occurence_list[$i]> $occurence_list[$i] </OPTION>";
		    }
			?>
		</SELECT>
		</div>

		<div class="suboptionsactivity">
		<label for="level">Level </label>
		<SELECT name='level' class = 'subformaddactivity'>  
		    <OPTION value=""> Select level </OPTION>
		    <OPTION> All levels </OPTION>
		    <OPTION> Good </OPTION>
		    <OPTION> Average </OPTION>
		    <OPTION> Beginner </OPTION>
		</SELECT>
		</div>
	</div>

	<label style="display:none" for="private_key">Private key </label>
	<input style="display:none" type='text' name='private_key' class = 'formaddactivity' placeholder="Leave empty if public"></input>

	<input 	type='submit' name='submit_activity' value='Submit' class=submitinput>

</form>

<!--
form output
	title_activity
	type_activity
	city_activity
	time_activity
	description_activity
	contact
	occurence
	level
	private_key
	submit_activity
-->

<script type="text/javascript">
$('#add_activity').validate({
// Specify validation rules

    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
	    title_activity: {
	      required: true,
	      minlength: 5
	    },
	    description_activity:{
	      	required: true,
	        minlength: 5
      	},
	    type_activity: "required",
	    city_activity: "required",
	    contact: "required",
		occurence: "required",
		level: "required"
    },
    // Specify validation error messages
    messages: {
      title_activity: {
      	required: "Please enter a title",
        minlength: "Your title is too short"
      },
	    type_activity: "required",
	    city_activity: "Please enter a city",
	    description_activity:{
      	required: "required",
        minlength: "Your title is too short"
      	},
	    contact: "required",
		occurence: "required",
		level: "required",
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
});
</script>