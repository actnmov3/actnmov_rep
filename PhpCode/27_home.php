<style type="text/css">
  .searchform{
    display:flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    margin: 3px;
    flex-wrap: wrap;
  }
  .input_actnmov{
    color: #5a5854;
    background: #f1f8ff;
    border: 1px solid #61E0FF;
    border-radius: 0px;
    padding: 5px;
    margin:  3px;
    text-align: left;
    width:140px;
    height: 36px;
  }

  .concept{
    display: flex;
    flex-direction: column;
    color: black;
      align-items: center;
      color: black;
      margin-top: 20px;
  }
</style>

<h1>Act'N'Mov</h1>
<p class="text1">Enjoy easy sharing sport!</p>
<form action="" method="get" class="searchform" id=form>
  <select name="sport" class="input_actnmov">
    <OPTION value=""> <?php echo "$sport_list[0]" ?> </OPTION>
    <?php
    $length = sizeof($sport_list)-1; 
    for ($i=1; $i<=$length; $i++){
      echo "<OPTION value= $sport_list[$i]> $sport_list[$i] </OPTION>";
    }
    ?> 
  </select>
  <!--<input id="searchTextField" type="search" name="location" placeholder="Enter city", value='Eindhoven' class="input_actnmov"/>-->
  <select name="location" class="input_actnmov">
    <OPTION value=""> <?php echo "$location_list[0]" ?> </OPTION>
    <?php
    $length = sizeof($location_list)-1; 
    for ($i=1; $i<=$length; $i++){
      echo "<OPTION value= $location_list[$i]> $location_list[$i] </OPTION>";
    }
    ?> 
  </select>
  <input  type="submit" name='search_activity' value="Search" class="submitinput" />     
</form>


<script type="text/javascript">
  /* attempt to play with jquery
  $(document).ready(function(){
    $('h1').click(function(e){
    console.log(e);
    });
    $('input').keyup(function(e){
      console.log(e.target.value);
    });
    $('#form').submit(function(e){
      e.preventDefault();
      var location = $('#searchTextField').val();
      if (location=='john') {
      console.log($('#searchTextField'))
      ;}
      });
  })
  */
</script>

<script type="text/javascript">
  //attempt to use maps API
  /*
  var input = document.getElementById('searchTextField');
  var options = { types: ['(cities)']  };
  autocomplete = new google.maps.places.Autocomplete(input, options);
  google.maps.event.addListener(autocomplete, 'place_changed', function(){
     var place = autocomplete.getPlace();
     if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
  })*/
</script>
