<style type="text/css">
	.top1{
		display:flex;
		flex-direction: row;
		justify-content: flex-start;	
		background: #61E0FF;
	}
	.formtop{
		display: flex;
		flex-direction: row;
  		justify-content: flex-start;
  		flex: 1;
	}
	.topright{
		display: flex;
		flex-direction: row;
  		justify-content: flex-end;
  		flex: 1;
	}
</style>

<div class="top1">
	<form action="" method="post" class="formtop">
		<input type="submit" name="home_display" value="Act'N'Mov"></input>
		<div class="topright">
			<input type="submit" name='add_activity' value="Add activity"></input>
			<input type="submit" name='my_account' value="My account"></input>
			<input type="submit" name='submit_loggout' value="Log out"></input>		
		</div>
	</form>
</div>