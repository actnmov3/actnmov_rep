<style type="text/css">
	.signin{
		display: flex;
		flex-direction: column;
    justify-content: center;
 		align-items: center;
  	flex-wrap: wrap;
  	width:90%;
	}
  .input_actnmov{
    color: #5a5854;
    background: #f1f8ff;
    border: 1px solid #61E0FF;
    border-radius: 0px;
    padding: 5px;
    margin:  3px;
    text-align: left;
    width: 100%;
    height: 36px;
  }
  .signin form{
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    max-width: 300px;
    width: 100%;
  }
  .signin label{
    color: #3a3a96; /*Blue*/
  }
  }
</style>


<div class="signin">
		<p style="font-weight: bold; text-align: center;">In order to add or manage activities, you need to be logged in.</p>
		<p>Already a member? Please log in:</p>
		<form action='' method='post' >
			<label for="email">Email adress</label>
			<input type='text' name='user_email' placeholder='e-mail' id='username' class="input_actnmov"> 
			<label for="password">Password</label>
			<input type='password'	name='password' placeholder='password' class="input_actnmov">
			<input type='submit' 	name='submit_loggin' value="Loggin" class='submitinput'>
		</form>
		<p>Not yet a member? Please subscribe:</p>
		<!--<button id="button_register">Register</button>-->
    <form   action="" method="post">
      <label for="new_username">Username</label>
      <input type="text" name="new_username" placeholder="Username (5 letters min)" value="<?php echo $new_username_zero ?>" class="input_actnmov" ></input>
      <label for="new_email">Email addres</label>
      <input type="email" name="new_email" placeholder="Email addres" value="<?php echo $new_email_zero ?>" class="input_actnmov"></input>
      <label for="new_password1">Password</label>
      <input id="password" type="password" name="new_password1" placeholder="Password" class="input_actnmov"></input>
      <label for="new_password2">Password confirmation</label>
      <input type="password" name="new_password2" placeholder="Password confirmation" class="input_actnmov"></input>
      <input  type="submit" name="register_1" value="Subscribe" class='submitinput'>
    </form>
	</div>

<script type="text/javascript">
$('#registerform').validate({
// Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      new_username: {
        required: true,
        minlength: 5
      },
      new_email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      new_password1: {
        required: true,
        minlength: 5
      },
      new_password2: {
      	equalTo: '#password'
      }
    },
    // Specify validation error messages
    messages: {
      new_username: {
      	required: "Please enter a username",
        minlength: "Your username must be at least 5 characters long"
      },
      new_password1: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      new_email: "Please enter a valid email address",
      new_password2 : "Both password must be equal"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
});

</script>

<script type="text/javascript">
$(document).ready(function(){
    $('#button_register').click(function(){
    	//$('#register').toggle();
    	//$('#loggin').toggle();
    });
})
</script>