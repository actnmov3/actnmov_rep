<style type="text/css">
	.activity_dsp{
		display: flex;
		flex-direction: column;
		margin-top: 3px;
		width: 80%;
	}
	.activity_row1{
		display: flex;
		flex-direction: row;
	    border-style: solid;
	    border-width: 1px 1px 1px 0px;
	    border-color: #61E0FF;
  		background-color: ;
  		padding-left: 0px;
  		padding-right: 0px;
	}
	.activity_col1{
		display: flex;
		flex-direction: column;
		width: 100%;
  		flex-wrap: wrap;
	}
	.activity_row11{
		display: flex;
		flex-direction: row;
		justify-content: flex-start;
  		align-items: flex-start;
  		flex-wrap: wrap;
	}
	.activity_ent1{
		display: flex;
		flex-direction: row;
  		align-items: flex-start;
  		flex-wrap: wrap;
		width: 100%;		
	}
	.activity_row12{
		display: flex;
		flex-direction: column;
		justify-content: flex-start;
  		align-items: flex-start;
  		flex-wrap: wrap;
  		flex-grow:1;
	    border-style: solid;
	    border-width: 0px 0px 0px 1px;
	    border-color: #61E0FF;
	}
	.activity_title{
		display: flex;
		flex-direction: row;
  		justify-content: flex-start; /* horizontal */
  		flex-wrap: wrap;
  		background-color: #61E0FF;
		width: 90%;
	}	
	.title{
		display: flex;
		flex-direction: row;
  		flex-wrap: wrap;
		color: #3a3a96;
	  	margin: 0px;
	  	font-family: "Arial", Verdana, Sans-Serif;
	  	background:none;
	  	padding:0;
	  	padding-left:5px;
	  	border:none;
	  	font-weight: bold;
	  	width: 100%;
	  }
	.text_tab{
		display: flex;
  		flex-wrap: wrap;
		color: #616161;
		margin-right: 5px;
		margin-left: 5px;
		margin-top: 0px;
		margin-bottom: 0px;
		font-family: "Arial", Verdana, Sans-Serif;
	  	background:none;
	  	padding:0;
	  	border:none;
	}
	.text_content{
		color: #3a3a96;
		margin-right: 5px;
		margin-left: 5px;
		margin-top: 0px;
		margin-bottom: 0px;
		font-family: "Arial", Verdana, Sans-Serif;
	  	background:none;
	  	padding:0;
	  	display: flex;
  		flex-wrap: wrap;	  	
	}
	.link{
		display: flex;
  		flex-wrap: wrap;
  		width: 100%;
		margin-right: 5px;
		margin-left: 5px;
		margin-top: 0px;
		margin-bottom: 0px;
		font-family: "Arial", Verdana, Sans-Serif;
	  	background:none;
	  	padding:0;
	  	border:none;
	}
</style>


<script>
function changedescription(num) {
	var description = document.getElementById("para-"+num).innerHTML;
	if (description.match('Description')) {
		document.getElementById("para-"+ num).innerHTML = "";
		document.getElementById("button_des"+ num).innerHTML = "Display description";
	} 
	else {
		document.getElementById("para-"+ num).innerHTML = 'Description : </br> $description';
		document.getElementById("button_des"+ num).innerHTML = "Close description";
	}
}
</script>


<?php

$test_tmp = "para-"."$count_activity" ;
$test_tmp2 = "button_des"."$count_activity" ;
//echo "$count_activity";
?>

<div class= activity_dsp  >
	<div class= activity_title>
		<a href="<?php echo "?activity_s=$ID" ?>" class="title"><?php echo "$title" ?></a>
	</div>
	<div class= activity_row1>
		<div class= activity_col1>
			<div class= activity_row12><p class= "text_tab">Description and time activity: </p> <p class="text_content"> <?php echo "$description" ?></p></div>
			<div class= activity_ent1>
				<div class= activity_row12><p class= "text_tab">Sport type </p> <p class="text_content"> <?php echo "$type" ?></p></div>
				<div class= activity_row12><p class= "text_tab">Level</p> 		<p class="text_content"> <?php echo "$level" ?></p></div>
				<div class= activity_row12><p class= "text_tab">Occurence </p> 	<p class="text_content"> <?php echo "$occurence" ?></p></div>
				<div class= activity_row12><p class="text_tab">City</p> <p class="text_content"><?php echo "$city" ?></p></div>
				<div class= activity_row12><p class= "text_tab">Organiser </p> 	<p class="text_content"> <?php echo "$username" ?></p></div>
			</div>
			<div class= activity_row12>
				<p class= "text_tab">Contact : </p> 
				<a  class="link" href="<?php echo "$contact" ?>" target="_blank"><?php echo "$contact" ?></a>
			</div>
		</div>
	</div>					
</div>


