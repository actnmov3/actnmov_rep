<?php // setupusers.php
 require_once 'PhpCode/login.php';
 $connection =  new mysqli($db_hostname, $db_username, $db_password, $db_database);
 if ($connection->connect_error) die($connection->connect_error);

$new_forename_zero=NULL;
$new_lastname_zero=NULL;
$new_username_zero=NULL;
$new_email_zero=NULL;
$valid_form_key=FALSE;

 if (isset($_POST['register_1'])){
$valid_form_key=TRUE;
//email check
	if (!isset($_POST['new_email'])){ echo 'Missing email address.</br>'; $valid_form_key=FALSE;}
	elseif($_POST['new_email']==NULL){ echo 'Missing email address</br>'; $valid_form_key=FALSE;}
	elseif (!filter_var($_POST["new_email"], FILTER_VALIDATE_EMAIL)) {echo 'Please enter a valid email address</br>'; $valid_form_key=FALSE;}
	else $new_email_zero=$_POST['new_email'];
//username check
	if (!isset($_POST['new_username'])){ echo 'Missing username.</br>'; $valid_form_key=FALSE;}
	elseif($_POST['new_username']==NULL){ echo 'Missing username</br>'; $valid_form_key=FALSE;}
	elseif (!preg_match("/^[a-zA-Z0-9]*$/",$_POST['new_username'])){echo 'Please enter a valid username..</br>'; $valid_form_key=FALSE;}
	else $new_username_zero=$_POST['new_username'];
//password an email double check
	if (!isset($_POST['new_password1'])){ echo 'Missing password.</br>'; $valid_form_key=FALSE;}
	if (!isset($_POST['new_password2'])){ echo 'Missing password..</br>'; $valid_form_key=FALSE;}
	elseif($_POST['new_password1']==NULL ){ echo 'Missing password</br>'; $valid_form_key=FALSE;}
	elseif ($_POST['new_password1']!=$_POST['new_password2']) { echo 'The two entered passwords are different</br>'; $valid_form_key=FALSE;}
	if ($valid_form_key==TRUE){
		$query = "SELECT * FROM user_attribute WHERE user_name = '$new_username_zero'";
		$result = $connection->query($query);
		if (!$result) die($connection->error);
		if ($result->num_rows){echo 'user already exists</br>'; $valid_form_key=FALSE;}
		$query = "SELECT * FROM user_attribute WHERE user_email = '$new_email_zero'";
		$result = $connection->query($query);
		if (!$result) die($connection->error);
		if ($result->num_rows){echo 'email address already exists </br>'; $valid_form_key=FALSE;}
		$result->close();
	}
	else echo "error in username addition </br>";
	if ($valid_form_key==TRUE) {
	$salt1 = "qm&h*";
	$salt2 = "pg!@";
	$email = $_POST['new_email'];
	$new_username = $_POST['new_username'];
	$password = $_POST['new_password1']; 	 
	$token = hash('ripemd128', "$salt1$password$salt2");
	add_user($connection, $new_username, $token, $email);
	$query="INSERT INTO history (username, action) VALUE ('$new_username','user creation');";
	$result = $connection->query($query);
	if (!$result) die($connection->error);
	
	echo "user $new_username added </br>";
	}		
	else echo "error in username addition";
}
if(isset($_POST['register_0']) or isset($_POST['check_username']) or $valid_form_key==FALSE){
	//echo "<p> Register Page </p>";
	require_once 'PhpCode/23_register.php';
}

 function add_user($connection, $un, $pw, $em)
 {
 $query = "INSERT INTO user_attribute (user_email, user_pwd, user_name) 
 	VALUES('$em' , '$pw','$un')";
 $result = $connection->query($query);
 if (!$result) die($connection->error);
 }
?>