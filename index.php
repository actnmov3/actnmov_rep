<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111853215-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-111853215-1');
    </script>

  <title>Actnmov</title>
  <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <!-- the responsive viewport meta tag -->
      <!-- Link to have bootstrap below (before all other stylesheets) -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
      <!-- Google map API -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSK0XIW2X2ZSyK87vCL7SJ9VoXGdlQD2M&libraries=places"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">


</head>

<?php
  require_once 'PhpCode/00_controller.php'; // control session // set $loggin_state & $display_state & $language
  require_once 'PhpCode/01_data_display.php'; // load text in correct language (EN)
?>

<body id="body">
  
    <?php 
    switch ($loggin_state) {
      case "logged_out":
        require_once 'PhpCode/26_top_loggedout.php';
          break;
      case "logged_in": //Activity search display
        require_once 'PhpCode/25_top_loggedin.php';
          break;
    } ?>
    
  <div id="mid1">
    <?php
      switch ($display_state) {
          case "home":
              require_once 'PhpCode/27_home.php';
              require_once 'PhpCode/29_concept.php';
              break;
          case "search": //Activity search display
              require_once 'PhpCode/27_home.php';
              require_once 'PhpCode/20_search_activities.php';
              break;
          case "add_activity": //Add activity page
              require_once 'PhpCode/14_add_activity.php';
              break;
          case "my_account": //My account page
              require_once 'PhpCode/22_my_account.php';
              //if ($username == "test5"){
            //require_once 'PhpCode/29_table_admin.php';
          //}
              break;
          case "sign_in": //Register page
              require_once 'PhpCode/12_register.php';
              break;
          case "concept": //Register page
              require_once 'PhpCode/29_concept.php';
              break;
          case "activity_s": //Register page
              require_once 'PhpCode/20_search_activities.php';
              break;
      }
      ?>
  </div>
  <div id="foot1" >
    <form action="?" method="post">
      <!--<input type="submit" name='contact' value="Contact us!"></input>-->
      <input type="submit" name='concept' value="Contact & Concept"></input>
    </form>
  </div> 

  <!-- Optional JavaScript -->
    
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

    
  </body>
</html>